# Simple cart application built with React & Redux

This simple application prototype shows how we can use React and Redux

## API

Loading api items from mockapi.io
link to json https://5e3c30f3f2cb300014391c39.mockapi.io/api/v1/item

## Features
* Add and remove of instance item
* Remove items
* Edit the quantity of the items in real time
* Calculate automatically the total including the shipping (if chosen)
* Order Form with validation
* Choose shipping delivery services
* Get your submit frorm

# Getting started
### Requirements

* Node.js
* NPM

### Package installation
```bash
npm install
```
 ### Start the React App
 Excute the following command: 
```bash
npm start
```
The application will start automatically in your browser on http://localhost:3000
