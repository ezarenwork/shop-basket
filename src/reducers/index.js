import {applyMiddleware, combineReducers, createStore} from "redux";
import thunkMiddleware from "redux-thunk";
import cart from "./cart"
import { reducer as formReducer } from 'redux-form'
import shipping from "./shipping";


const store = createStore(combineReducers({
    cart,
    form: formReducer,
    shipping
}), applyMiddleware(thunkMiddleware));

export default store;

window.store = store;