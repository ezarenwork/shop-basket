import {connect} from "react-redux";
import Cart from "../components/Cart";


const mapStateToProps = (state) => ({
    products: state.cart.products,
    price: state.cart.price,
    isReady: state.cart.isReady
});

export default connect(mapStateToProps)(Cart)
