import React from 'react';
import {connect} from "react-redux";
import Shipping from "../components/Shipping";
import {setSubmitData} from "../actions/shipping";


const ShippingContainer = (props) => {
    const onSubmitForm = (submitData) => {
        props.setSubmitData(submitData);
        alert("Your order has been sent for processing\n" +
            'name: ' + submitData.name + '\n' +
            'address: ' + submitData.address + '\n' +
            'phone: ' + submitData.phone + '\n' +
            'email: ' + submitData.email + '\n' +
            'shipping price: ' + submitData.shippingMethod);
    };

    return <Shipping price={props.price} products={props.products} onSubmit={onSubmitForm}/>
};

const mapStateToProps = (state) => ({
    price: state.cart.price,
    products: state.cart.products,
    submitData: state.shipping.submitData
});

export default connect(mapStateToProps, {setSubmitData})(ShippingContainer)
