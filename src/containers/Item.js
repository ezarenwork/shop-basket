import {connect} from "react-redux";
import Item from "../components/Item";
import {deleteItemFromCart, updateCount} from "../thunks/cart";


const mapStateToProps = (state, {product}) =>({
    count: product.count
});
export default connect(mapStateToProps, {
    deleteItemFromCart,
    updateCount
})(Item)
