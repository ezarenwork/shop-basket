import React from 'react';
import cartItem from "../assets/img/cart.png"
import style from "../styles/item.module.css"
import Stepper from "./Stepper";


const Item = ({product, count, deleteItemFromCart, updateCount, priceCount}) => {
    return (
        <li className={style.item} key={product.id}>
            <div className={style.link}>
                <img src={product.image != null ? product.image : cartItem}
                     alt=''/>
            </div>
            <div className={style.description}>
                <h2>
                    {product.name}
                </h2>
                <div>
                    {product.color}
                </div>
                <div>
                    {product.material}
                </div>
            </div>
            <div className={style.rightBlock}>
                <div className={style.quantity}>
                    <Stepper count={count}
                             product={product}
                             updateCount={updateCount}
                             priceCount={priceCount}
                    />
                </div>
                <div className={style.price}>
                    <button
                        type="button" onClick={deleteItemFromCart.bind(this, product)}>
                    </button>
                    <div>
                        {product.price} &#8364;
                    </div>
                </div>
            </div>
        </li>
    )
};
export default Item;


