export const required = value => (value || typeof value === 'number' ? undefined : 'Required');

const maxLength = max => value =>
  value && value.length > max ? `Must be ${max} characters or less` : undefined;

const minLength = min => value =>
  value && value.length < min ? `Must be ${min} characters or more` : undefined;

export const email = value =>
  value && !/^([a-zA-Z0-9_\-.]+)@([a-zA-Z0-9_\-.]+)\.([a-zA-Z]{2,5})$/i.test(value)
    ? 'Invalid email address'
    : undefined;

export const alphaNumeric = value =>
    value && /[^a-zA-Z0-9А-Яа-я ]/i.test(value)
        ? 'Only alphanumeric characters'
        : undefined;

export const phoneNumber = value =>
    value && !/^\+?3?8?(0\d{9})$/i.test(value)
        ? 'Invalid phone number'
        : undefined;


export const maxLength50 = maxLength(50);
export const maxLength30 = maxLength(30);
export const minLength3 = minLength(3);
